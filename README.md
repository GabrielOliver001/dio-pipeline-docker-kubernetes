### Pipeline de Deploy de Aplicação com GitLab CI/CD, Docker, Kubernetes (GKE) e Terraform

Este projeto demonstra como configurar um pipeline de CI/CD no GitLab para construir, testar e implantar uma aplicação Docker em um cluster Kubernetes no Google Kubernetes Engine (GKE) com Bastion Host. A infraestrutura do Kubernetes é provisionada usando Terraform. O projeto é organizado em duas branches: `DEV` para o ambiente de desenvolvimento e `PROD` para o ambiente de produção.

## Estrutura do Projeto

- **app/**: Diretório contendo o código da aplicação.
- **.gitlab-ci.yml**: Arquivo de configuração do GitLab CI/CD.
- **Dockerfile**: Arquivo de configuração do Docker para construir a imagem da aplicação.
- **terraform/**: Diretório contendo os arquivos de configuração do Terraform para provisionar a infraestrutura do Kubernetes e o Bastion Host.

## Pré-requisitos

- Conta no GitLab
- Docker instalado
- Kubectl instalado
- Terraform instalado
- Cluster Kubernetes (GKE) configurado
- Bastion Host configurado

## Configuração do Projeto

### 1. Configuração do Terraform

1. **Instale o Terraform**:
   Siga as instruções em [Terraform Downloads](https://www.terraform.io/downloads.html) para instalar o Terraform.

2. **Configure os arquivos do Terraform**:
   No diretório `terraform/`, configure os arquivos `.tf` para provisionar a infraestrutura necessária.

3. **Inicie e aplique o Terraform**:

   ```sh
   cd terraform
   terraform init
   terraform apply
   ```

### 2. Configuração do Docker

1. **Crie um Dockerfile**:
   No diretório raiz do projeto, crie um `raiz` para a aplicação:

2. **Construa e teste a imagem localmente**:


### 3. Configuração do GitLab CI/CD

1. **Crie o arquivo `.gitlab-ci.yml`**:
   No diretório raiz, crie o arquivo `.gitlab-ci.yml`:
    


### Executar o Pipeline

1. **Crie e faça commit das mudanças nas branches apropriadas**:

   Para desenvolvimento:
   ```sh
   git checkout -b dev
   git add .
   git commit -m "Configuração inicial do pipeline CI/CD para DEV"
   git push origin dev
   ```

   Para produção:
   ```sh
   git checkout -b prod
   git add .
   git commit -m "Configuração inicial do pipeline CI/CD para PROD"
   git push origin prod
   ```

2. **Verificar o pipeline**:
   Vá até a página do projeto no GitLab e verifique os pipelines em execução em CI/CD > Pipelines.

## Conclusão

Com essas configurações, seu pipeline CI/CD no GitLab será capaz de construir, testar e implantar a aplicação Docker em um cluster Kubernetes no GKE, utilizando um Bastion Host para acessar o cluster. A infraestrutura é provisionada usando Terraform, garantindo uma configuração reproduzível e escalável.